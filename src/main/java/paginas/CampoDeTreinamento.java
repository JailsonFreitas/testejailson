package paginas;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CampoDeTreinamento {

	@Ignore
	@Test
	//comando para chamar o driver do Chrome
	public void cadastroSimples() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jailson Freitas\\Desktop\\chromedriver.exe");

	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	//caminho da pagina que vai ser testada
	driver.get("file:///C:/Users/Jailson%20Freitas/Downloads/componentes.html");
	

	WebElement nome = driver.findElement(By.id("elementosForm:nome")); 
	//Esta varievel WebElement iremos dizer quais s�o as ID
	WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
	WebElement sexoM = driver.findElement(By.id("elementosForm:sexo:0"));
	WebElement comida = driver.findElement(By.id("elementosForm:comidafavorita:0"));
	WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
	WebElement cadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
	
	/*este param�tro ir� inserir os dados aonde voc� escolheu
	por regra de programa��o sempre colocar o nome da variavel e limpar */
	nome.clear();
	nome.sendKeys("Jailson");
	
	sobrenome.clear();
	sobrenome.sendKeys("Freitas");
	
	sexoM.click();
	
	comida.click();
	
	// sempre que tiver combobox fazer este procedimento
	WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
	Select combo = new Select(element);
	combo.selectByValue("superior");
	Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());
	
	WebElement element2 = driver.findElement(By.id("elementosForm:esportes"));
	Select combo2 = new Select(element2);
	combo2.selectByValue("Corrida");
	Assert.assertEquals("Corrida", combo2.getFirstSelectedOption().getText());
	
	
	
	sugestoes.clear();
	sugestoes.sendKeys("Teste Funfou!");
	
	cadastrar.click();
	
//	driver.quit();
	
	
	}
	
	@Test
	
	public void BrincarAlert() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jailson Freitas\\Desktop\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//caminho da pagina que vai ser testada
		driver.get("file:///C:/Users/Jailson%20Freitas/Downloads/componentes.html");
		
		WebElement clickAlert = driver.findElement(By.xpath("//table[@id='elementosForm:tableUsuarios']//tr[1]/td[3]/input"));
		
		clickAlert.click();
		
		Alert  alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Francisco", texto);
		alert.accept();
		
		driver.quit();
	}
}
