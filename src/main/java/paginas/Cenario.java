package paginas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Cenario {
	
	@Test
	public void cenario() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jailson Freitas\\Desktop\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//caminho da pagina que vai ser testada
		driver.get("file:///C:/Users/Jailson%20Freitas/Downloads/componentes.html");
		
		//vai selecionar e clicar em cadastrar como n tem dados tem que validar 
		WebElement cadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
		cadastrar.click();//ok
		
		Alert  alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Nome eh obrigatorio", texto);
		alert.accept();
		driver.switchTo().window("");
		
		String nome1 = "Jailson";
		String sobrenome1 = "Freitas";
		String sexo1 ="Masculino";
		String comida1 ="Carne";
		String escolaridade1 ="Superior";
		String esportes1 ="Corrida";
		String sugestoes1 ="Teste Funfou!";
		
		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		WebElement sexoM = driver.findElement(By.id("elementosForm:sexo:0"));
		WebElement comida = driver.findElement(By.id("elementosForm:comidafavorita:0"));
		WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		WebElement cadastrar1 = driver.findElement(By.id("elementosForm:cadastrar"));
		
		
		
		nome.clear();
		nome.sendKeys("Jailson");
		assertTrue(nome1.equals("Jailson"));
		
		
		
		sobrenome.clear();
		sobrenome.sendKeys("Freitas");
		assertTrue(sobrenome1.equals("Freitas"));
		
		sexoM.click();
		assertTrue(sexo1.equals("Masculino"));
		
		comida.click();
		assertTrue(comida1.equals("Carne"));
		
		// sempre que tiver combobox fazer este procedimento
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		combo.selectByValue("superior");
		Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());
		assertTrue(escolaridade1.equals("Superior"));
		WebElement element2 = driver.findElement(By.id("elementosForm:esportes"));
		Select combo2 = new Select(element2);
		combo2.selectByValue("Corrida");
		Assert.assertEquals("Corrida", combo2.getFirstSelectedOption().getText());
		assertTrue(esportes1.equals("Corrida"));
		
		
		sugestoes.clear();
		sugestoes.sendKeys("Teste Funfou!");
		assertTrue(sugestoes1.equals("Teste Funfou!"));
		cadastrar1.click();
		
		driver.quit();
		
	}

}
